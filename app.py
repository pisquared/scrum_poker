from flask import Flask, render_template, request, session, redirect, url_for, flash
from flask_socketio import SocketIO, join_room, send, emit
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
socketio = SocketIO(app)
db = SQLAlchemy(app)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))


class Vote(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Integer)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', backref=db.backref('votes', lazy='dynamic'))
    session_id = db.Column(db.Integer, db.ForeignKey('session.id'))
    session = db.relationship('Session', backref=db.backref('votes', lazy='dynamic'))


class Session(db.Model):
    id = db.Column(db.Integer, primary_key=True)


class Room(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True)
    current_session_id = db.Column(db.Integer, db.ForeignKey('session.id'))
    current_session = db.relationship('Session',
                                      backref=db.backref('room'),
                                      foreign_keys=[current_session_id],
                                      uselist=False)
    previous_session_id = db.Column(db.Integer, db.ForeignKey('session.id'))
    previous_session = db.relationship('Session',
                                       backref=db.backref('ex_room'),
                                       foreign_keys=[previous_session_id],
                                       uselist=False)


def save_user_to_session(user_name="Anonymous"):
    if 'user_id' in session and session['user_id']:
        user = User.query.filter_by(id=session['user_id']).first()
        return user
    user = None
    if not user:
        user = User(name=user_name)
        db.session.add(user)
        db.session.commit()
    session['user_id'] = user.id
    return user


@app.route('/')
def index():
    rooms = Room.query.all()
    return render_template('index.html', rooms=rooms)


@app.route('/rooms/enter', methods=['POST'])
def rooms_enter():
    room_name = request.form.get('room_name')
    user_name = request.form.get('user_name')
    session['user_id'] = None
    save_user_to_session(user_name)
    room = Room.query.filter_by(name=room_name).first()
    if not room:
        flash('Unknown room')
        return redirect(url_for('index'))
    return redirect(url_for('rooms', room_id=room.id))


@app.route('/rooms/<int:room_id>')
def rooms(room_id):
    user = save_user_to_session()
    room = Room.query.filter_by(id=room_id).first()
    return render_template('room.html', room=room, user=user)


@app.route('/rooms/create', methods=['POST'])
def rooms_create():
    save_user_to_session()
    room_name = request.form.get('room_name')
    current_session = Session()
    room = Room(name=room_name, current_session=current_session)
    db.session.add(room)
    db.session.commit()
    return redirect(url_for('rooms_stage', room_id=room.id))


@app.route('/rooms/<int:room_id>/stage')
def rooms_stage(room_id):
    user = save_user_to_session()
    room = Room.query.filter_by(id=room_id).first()
    return render_template('room_stage.html', room=room)


@socketio.on('new_user')
def handle_new_user(message):
    room = message['room']
    print('new_user: {}'.format(message))


@socketio.on('join')
def handle_join(message):
    user_id = session['user_id']
    room_id = message['room_id']
    join_room(room_id)
    send('{} has entered the room {}'.format(user_id, room_id))


@socketio.on('vote')
def handle_vote(message):
    user_id = session['user_id']
    user = User.query.filter_by(id=user_id).first()
    vote_value = message['vote_value']
    room_id = message['room_id']
    room = Room.query.filter_by(id=room_id).first()
    current_session = room.current_session
    existing_vote = Vote.query.filter_by(user_id=user_id, session=current_session).first()
    if existing_vote:
        db.session.delete(existing_vote)
    vote = Vote(user_id=user_id, value=vote_value, session=current_session)
    db.session.add(vote)
    db.session.commit()
    message = {
        'user_id': user_id,
        'user_name': user.name,
        'vote_value': vote_value,
    }
    emit('vote', message, room=room_id)


@socketio.on('reveal')
def handle_reveal(message):
    room_id = message['room_id']
    room = Room.query.filter_by(id=room_id).first()
    room.previous_session = room.current_session
    room.current_session = None
    db.session.add(room)
    db.session.commit()


@socketio.on('new_session')
def handle_new_session(message):
    room_id = message['room_id']
    room = Room.query.filter_by(id=room_id).first()
    if room.current_session:
        room.previous_session = room.current_session
    room.current_session = Session()
    db.session.add(room)
    db.session.commit()


if __name__ == '__main__':
    socketio.run(app, debug=True)
